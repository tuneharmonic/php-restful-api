<?php

include_once 'validationResult.php';

class Validator {
    public static function isValidJson(string $str) : ValidationResult {
        $valid = true;
        $reasons = ['Valid'];

        if (strlen($str) > 0) {
            // $str = utf8_encode($str);
            json_decode($str);
            $valid = json_last_error() == JSON_ERROR_NONE;
            $reasons = [json_last_error_msg()];
        } else {
            $valid = false;
            $reasons = ['Provided string is empty'];
        }

        return new ValidationResult(boolval($valid), $reasons);
    }

    public static function isValidPerson(stdClass $obj) : ValidationResult {
        $valid = true;
        $reasons = [];

        if (!empty($obj)) {
            if (!property_exists($obj, 'id') || empty($obj->id)) {
                $valid = false;
                $reasons[] = '"id" property is empty or missing';
            } else if (gettype($obj->id) != 'integer') {
                $valid = false;
                $reasons[] = '"id" property must be an integer';
            }

            if (!property_exists($obj, 'name') || empty($obj->name)) {
                $valid = false;
                $reasons[] = '"name" property is empty or missing';
            } else if (gettype($obj->name) != 'string') {
                $valid = false;
                $reasons[] = '"name" property must be a string';
            }

            if (!property_exists($obj, 'gender') || empty($obj->gender)) {
                $valid = false;
                $reasons[] = '"gender" property is empty or missing';
            } else if (gettype($obj->gender) != 'string') {
                $valid = false;
                $reasons[] = '"gender" property must be a string';
            }

            if (!property_exists($obj, 'age') || empty($obj->age)) {
                $valid = false;
                $reasons[] = '"age" property is empty or missing';
            } else if (gettype($obj->age) != 'integer') {
                $valid = false;
                $reasons[] = '"age" property must be a string';
            }
        } else {
            $valid = false;
            $reasons[] = 'object instance is null or empty';
        }

        if ($valid) {
            $reasons[] = 'Valid';
        }

        return new ValidationResult($valid, $reasons);
    }
}

?>