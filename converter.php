<?php

include_once 'person.php';

class Converter {
    public static function convertToPerson(stdClass $proto) : Person {
        return new Person($proto->id, $proto->name, $proto->gender, $proto->age);
    }
}

?>