<?php

include_once 'person.php';

interface IPersonStore {
    public function getPeople() : array;
    public function getPerson(int $id) : Person;
    public function addPerson(Person $person) : array;
    public function editPerson(Person $person) : array;
    public function removePerson(int $id) : array;
}

?>