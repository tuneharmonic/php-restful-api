<?php
// Let's make an API!
include_once 'personStoreFactory.php';
include_once 'validator.php';
include_once 'converter.php';


function exception_error_handler($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");

try {
    // Init Person store
    $personStore = PersonStoreFactory::createStore();
    
    // Required headers for the response
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Accept: text/json");

    // Determine action, parse inputs, execute, return response
    $method = $_SERVER['REQUEST_METHOD'];
    $response = $personStore->getPeople();  // fall through

    if ($method == 'GET') {                                         // GET
        if (!empty($_GET['id'])) {                                  //  ?id=[number] - return one
            $myId = intval($_GET['id']);
            $response = $personStore->getPerson($myId);
        }
        // GET all - return all - no code required!

    } else if ($method == 'POST') {                                 // POST person - return all
        $raw = file_get_contents("php://input");
        if (is_string($raw)) {
            $jsonValResult = Validator::isValidJson($raw);
            if ($jsonValResult->valid) {
                $personProto = json_decode($raw);
                $personValResult = Validator::isValidPerson($personProto);
                if ($personValResult->valid) {
                    $person = Converter::convertToPerson($personProto);
                    $response = $personStore->addPerson($person);
                } else {
                    $response = $personValResult;
                }
            } else {
                $response = $jsonValResult;
            }
        }
        
    } else if ($method == 'PUT') {                                  // PUT person - return all
        $raw = file_get_contents("php://input");
        if (is_string($raw)) {
            $jsonValResult = Validator::isValidJson($raw);
            if ($jsonValResult->valid) {
                $personProto = json_decode($raw);
                $personValResult = Validator::isValidPerson($personProto);
                if ($personValResult->valid) {
                    $person = Converter::convertToPerson($personProto);
                    $response = $personStore->editPerson($person);
                } else {
                    $response = $personValResult;
                }
            } else {
                $response = $jsonValResult;
            }
        }

    } else if ($method == 'DELETE') {                               // DELETE ?id=[number] - return all
        if (!empty($_GET['id'])) {
            $myId = intval($_GET['id']);
            $response = $personStore->removePerson($myId);
        }
    }

    echo json_encode($response);

} catch (Exception $e) {
    echo $e->getMessage();
}


?>