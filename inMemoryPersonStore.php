<?php

include_once 'person.php';
include_once 'iPersonStore.php';

class InMemoryPersonStore implements IPersonStore {
    // Index
    private $currentId;
    // In-mem collection of Persons
    private $people;

    function __construct() {
        if (empty($this->people)) {
            $this->people = array(
                new Person(1, 'Kyle', 'male', 27),
                new Person(2, 'Ryan', 'male', 28),
                new Person(3, 'Barb', 'female', 60)
            );
        }
        $this->currentId = 3;
    }
    
    // utility functions
    public function getPeople() : array {
        return $this->people;
    }

    public function getPerson(int $id) : Person {
        $myPerson = null;
        foreach ($this->people as $person) {
            if ($person->id == $id) {
                $myPerson = $person;
            }
        }
        unset($person);
        return $myPerson;
    }
    
    public function addPerson(Person $person) : array {
        $person->id = $this->getNextId();
        $this->people[] = $person;
        return $this->people;
    }
    
    public function editPerson(Person $person) : array {
        foreach ($this->people as &$origPerson) {
            if ($origPerson->id == $person->id) {
                $origPerson = $person;
                break;
            }
        }
        return $this->people;
    }
    
    public function removePerson(int $id) : array {
        $myIndex = -1;
        for ($i=0; $i < count($this->people); $i++) { 
            if ($this->people[$i]->id == $id) {
                $myIndex = $i;
                break;
            }
        }
        if ($myIndex != -1) {
            unset($this->people[$myIndex]);
            $this->people = array_values($this->people);
        }
        return $this->people;
    }

    private function getNextId() : int {
        return ++$this->currentId;
    }
}

?>