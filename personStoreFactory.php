<?php

include_once 'iPersonStore.php';
include_once 'inMemoryPersonStore.php';
include_once 'mySqlPersonStore.php';

class PersonStoreFactory {
    public static function createStore() : IPersonStore {
        $isDevelopment = !array_key_exists('VCAP_SERVICES', $_ENV);

        if ($isDevelopment) {
            return new InMemoryPersonStore();
        } else {
            $service_blob = json_decode($_ENV['VCAP_SERVICES'], true);
            $mysql_services = array();
            foreach($service_blob as $service_provider => $service_list) {
                // looks for 'cleardb' or 'p-mysql' service
                if ($service_provider === 'cleardb' || $service_provider === 'p-mysql') {
                    foreach($service_list as $mysql_service) {
                        $mysql_services[] = $mysql_service;
                    }
                    continue;
                }
            }

            if (count($mysql_services) > 0) {
                $creds = $mysql_services[0]['credentials'];
            } else {
                throw new Exception('Data service cannot be determined...');
            }

            return new MySqlPersonStore($creds['hostname'], $creds['username'], $creds['password'], $creds['name']);
        }
    }
}

?>