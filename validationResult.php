<?php

class ValidationResult {
    public $valid;
    public $reasons;

    function __construct(bool $valid, array $reasons) {
        $this->valid = $valid;
        $this->reasons = $reasons;
    }
}

?>