<?php

include_once 'person.php';
include_once 'iPersonStore.php';

class MySqlPersonStore implements IPersonStore {
    private $conn;
    private $dbname;
    
    function __construct($servername, $username, $password, $dbname) {
        $this->conn = new mysqli($servername, $username, $password);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $this->dbname = $dbname;
        // $this->ensureDbCreated();
        $this->useDatabase();
        $this->ensureTableCreated();
    }

    function __destruct() {
        $this->conn->close();
    }

    private function ensureDbCreated() {
        $sql = "CREATE DATABASE IF NOT EXISTS $this->dbname";

        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }
    }

    private function useDatabase() {
        $sql = "USE $this->dbname";

        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }
    }

    private function ensureTableCreated() {
        $sql = "CREATE TABLE IF NOT EXISTS People (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        gender VARCHAR(30) NOT NULL,
        age INT(6) UNSIGNED NOT NULL
        )";

        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }
    }

    public function getPeople() : array {
        $people = array();
        $sql = "SELECT * FROM People";
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $id = intval($row["id"]);
                $name = $row["name"];
                $gender = $row["gender"];
                $age = intval($row["age"]);

                $people[] = new Person($id, $name, $gender, $age);
            }
        }
        return $people;
    }

    public function getPerson(int $id) : Person {
        $person = new Person($id, 'Joe', 'male', 22);

        $sql = "SELECT * FROM People WHERE id = $id";
        $result = $this->conn->query($sql);
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();

            $id = intval($row["id"]);
            $name = $row["name"];
            $gender = $row["gender"];
            $age = intval($row["age"]);

            return new Person($id, $name, $gender, $age);
        } else {
            throw new Exception("Person with id $id cannot be found");
        }
    }

    public function addPerson(Person $person) : array {
        $sql = "INSERT INTO People (name, gender, age) VALUES ($person->name, $person->gender, $person->age)";
        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }

        return $this->getPeople();
    }

    public function editPerson(Person $person) : array {
        $sql = "UPDATE People SET name = $person->name, gender = $person->gender, age = $person->age WHERE id = $person->id";
        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }

        return $this->getPeople();
    }

    public function removePerson(int $id) : array {
        $sql = "DELETE FROM People WHERE id = $id";
        $result = $this->conn->query($sql);
        if ($result !== TRUE) {
            throw new Exception($this->conn->error);
        }

        return $this->getPeople();
    }
}

?>