<?php

class Person {
    public $id;
    public $name;
    public $gender;
    public $age;

    function __construct($id, $name, $gender, $age) {
        $this->id = $id;
        $this->name = $name;
        $this->gender = $gender;
        $this->age = $age;
    }
}

?>